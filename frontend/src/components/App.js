import React, {Component} from 'react';
import { render } from "react-dom";
import './App.css';
import Game from "./game/Game";
import IndexWithRouter from "./index/Index";
import {BrowserRouter} from "react-router-dom";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            type: props.type,
            cards: [],
        };
        
        this.drawIndex = this.drawIndex.bind(this);
        this.drawGame = this.drawGame.bind(this);
    }
    
    componentDidMount() {
        
    }
    
    drawIndex() {
        return (
            <div className="App">
                <BrowserRouter>
                    <IndexWithRouter />
                </BrowserRouter>
            </div>
        );
    }
    
    drawGame() {
        return (
            <div className="App">
                    <Game />
            </div>
        );
    }

    render() {
        switch (this.state.type) {
            case "index":
                return this.drawIndex();
            case "game":
                return this.drawGame();
            default:
                return <span>ERROR.</span>
        }
    }
}

export default App;

const index_container = document.getElementById("index");
if (index_container) {
    render(<App type={"index"} />, index_container);
}
else {
    const game_container = document.getElementById("game");
    if (game_container) {
        render(<App type={"game"} />, game_container);
    }
}
