import React, {Component} from 'react';
import {Redirect, withRouter} from "react-router";
import './Index.css';
import {Input, Button} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import CreateGameFormWithRouter from "./CreateGameForm";

class Index extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            err: false,
            created: false,
            id: ""
        };
        
        this.createGame = this.createGame.bind(this);
    }

    componentDidMount() {
    }
    
    createGame(id) {
        fetch("api/createGame?",  {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                game_id: id,
            })
        }).then(response => {
            if (response.status >= 400) {
                this.setState({
                    err: true,
                    created: false,
                    id: ""
                });
            }
            else {
                this.setState({
                    created: true,
                    id: id
                });
            }
        });
    }

    render() {
        return <Grid container justify="center" spacing={5}>
            <Grid item xs={12} style={{height: "5%"}}/>
            <CreateGameFormWithRouter/>
        </Grid>
    }
}

const IndexWithRouter = withRouter(Index);

export default IndexWithRouter;
