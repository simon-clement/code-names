import React, {Component} from 'react';
import {Redirect, withRouter} from "react-router";
import './CreateGameForm.css';
import {Input, Button} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";

class CreateGameForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            err: false,
            creating: false,
            created: false,
            id: ""
        };

        this.createGame = this.createGame.bind(this);
        this.created = this.created.bind(this);
    }

    componentDidMount() {
    }

    createGame(id) {
        fetch("api/createGame?",  {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                game_id: id,
            })
        }).then(response => {
            if (response.status >= 400) {
                this.setState({
                    err: true,
                    creating: false,
                    created: false,
                    id: ""
                });
            }
            else {
                this.setState({
                    creating: false,
                    created: true,
                    id: id
                });
            }
        });
    }
    
    created() {
        return this.state.created && this.state.id !== "";
    }

    render() {
        if (this.state.creating) {
            return (
                <Grid style={{width:"50%"}} container justify="center" spacing={1} className="CreateGameForm">
                    <Grid item xs={7} style={{height: "50px"}} />
                    <Grid item xs={7}>
                        <span>Création...</span>
                    </Grid>
                    <Grid item xs={7} style={{height: "50px"}} />
                </Grid>
            );
        }
        
        return (
            <Grid style={{width:"50%"}} container justify="center" spacing={1} className="CreateGameForm">
                <Grid item xs={7}>
                    {this.state.err && <span style={{color: "red"}}>Une erreur est survenue</span>}
                </Grid>
                <Grid item xs={7}>
                    {
                        !this.created() &&
                        <Input fullWidth onChange={(event) => this.setState({id: event.target.value})} />
                    }
                    {
                        this.created() &&
                        <span>La partie a été créée !</span>
                    }
                </Grid>
                <Grid item xs={7}>
                    {
                        !this.created() &&
                        <Button onClick={() => {this.setState({creating: true}); this.createGame(this.state.id);}} variant="contained" color="primary">
                            Creer une partie
                        </Button>
                    }
                    {
                        this.created() &&
                        <Button href={'/game?game_id=' + this.state.id} variant="contained" color="primary">
                            Rejoindre la partie
                        </Button>
                    }
                </Grid>
            </Grid>
        );
    }
}

const CreateGameFormWithRouter = withRouter(CreateGameForm);

export default CreateGameFormWithRouter;
