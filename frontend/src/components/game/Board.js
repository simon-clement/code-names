import React from "react";
import './Board.css'
import PropTypes from 'prop-types'
import Grid from "@material-ui/core/Grid";
import Card from "./Card";

class Board extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            gameId: props.gameId,
            sendReveal: props.sendReveal
        }
    }

    // static propTypes = {
    //     gameId: PropTypes.string.isRequired,
    //     cards: PropTypes.array.isRequired,
    //     isSpy: PropTypes.bool.isRequired,
    //     playersTurn: PropTypes.bool.isRequired,
    // };
    //
    // static defaultProps = {
    //     revealed: false
    // };

    render() {
        let rows = [];
        for (let r = 0; r < 5; r++) {
            let cards = [];
            for (let c = 0; c < 5; c++) {
                let card = this.props.cards[5*r + c];
                cards.push(
                    <Grid key={c} item xs={2}>
                        <Card isSpy={this.props.isSpy} 
                              showColor={this.props.showColor}
                              color={card.color} 
                              word={card.word} 
                              playersTurn={this.props.playersTurn} 
                              revealed={card.revealed}
                              sendReveal={this.state.sendReveal} />
                    </Grid>);
            }
            rows.push(cards);
        }
        
        return <Grid container justify="center" spacing={1}>
            {rows.map((r, i) =>
            <Grid key={i} container item justify="center" xs={12} spacing={1}>
                {r.map(c => c)}
            </Grid>)}
        </Grid>;
    }
}

export default Board;