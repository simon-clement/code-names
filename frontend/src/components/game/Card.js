import React from "react";
import './Card.css'

class Card extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            revealed: props.revealed,
            showWord: !props.revealed,
            sendReveal: props.sendReveal
        };
        
        this.mouseEnter = this.mouseEnter.bind(this);
        this.mouseLeave = this.mouseLeave.bind(this);
        this.click = this.click.bind(this);
    }

    // static propTypes = {
    //     word: PropTypes.string.isRequired,
    //     color: PropTypes.string.isRequired,
    //     revealed: PropTypes.bool.isRequired,
    //     isSpy: PropTypes.bool.isRequired,
    //     playersTurn: PropTypes.bool.isRequired,
    // };
    //
    // static defaultProps = {
    //     revealed: false
    // };
    
    mouseEnter() {
        if (this.state.revealed) {
            this.setState({showWord: true})
        }
    };

    mouseLeave() {
        if (this.state.revealed) {
            this.setState({showWord: false})
        }
    };
    
    click() {
        if (!this.state.revealed) {
            this.state.sendReveal(this.props.word);
        }
    };
    
    componentDidUpdate(prevProps) {
        if (this.props.revealed !== prevProps.revealed) {
            this.setState({
                revealed: this.props.revealed,
                showWord: !this.props.revealed
            })
        }
    }

    render() {
        return <div className={"card" + " " + (this.state.showWord ? "" : this.props.color) + " "
                    + (!this.props.isSpy && this.props.playersTurn && !this.state.revealed ? "selectable" : "")}
                    onMouseEnter={this.mouseEnter}
                    onMouseLeave={this.mouseLeave}
                    onClick={this.props.isSpy || !this.props.playersTurn ? () => {} : this.click}>
            <span className={(this.state.showWord ? "" : "invisible-") + "word" + " " + (this.props.isSpy || this.props.showColor ? this.props.color + "-spy" : "")}>
                {this.props.word}
            </span>
        </div>
    }
}

export default Card;