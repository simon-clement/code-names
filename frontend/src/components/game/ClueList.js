﻿import React, {Component} from "react";
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";

import "./ClueList.css"

class ClueList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const blueClues = this.props.clues.filter(c => c.color === "blue");
        const redClues = this.props.clues.filter(c => c.color === "red");
        let rows = [];
        for (let i = 0; i < Math.max(blueClues.length, redClues.length); i++) {
            rows.push([blueClues[i], redClues[i]]);
        }

        return (
            <div className={"ClueList"} >
                <h2>Indices</h2>
                <TableContainer component={"span"}>
                    <Table aria-label="clues">
                        <TableBody>
                            {
                                rows.map((row, i) =>
                                    <TableRow key={i}>
                                        <TableCell align="center" className={"blue-cell word-cell"}>
                                            {row[0] ? row[0].word : ""}
                                        </TableCell>
                                        <TableCell align="center" className={"blue-cell number-cell"}>
                                            {row[0] ? row[0].number === -1 ? "inf" : row[0].number : ""}
                                        </TableCell>

                                        <TableCell align="center" className={"red-cell word-cell"}>
                                            {row[1] ? row[1].word : ""}
                                        </TableCell>
                                        <TableCell align="center" className={"red-cell number-cell"}>
                                            {row[1] ? row[1].number === -1 ? "inf" : row[1].number : ""}
                                        </TableCell>
                                    </TableRow>
                                )
                            }
                        </TableBody>
                    </Table>
                </TableContainer>
            </div>
        );
    }
}

export default ClueList;