﻿import React, {Component} from "react";
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";

import "./PlayerList.css"

class PlayerList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const bluePlayers = this.props.players.filter(p => p.color === "blue");
        const redPlayers = this.props.players.filter(p => p.color === "red");
        let rows = [];
        for (let i = 0; i < Math.max(bluePlayers.length, redPlayers.length); i++) {
            rows.push([bluePlayers[i], redPlayers[i]]);
        }
        
        return (
            <div className={"PlayerList"} >
                <h2>Équipes</h2>
                <TableContainer component={"span"}>
                    <Table aria-label="players">
                        <TableBody>
                            {
                                rows.map((row, i) =>
                                    <TableRow key={i}>
                                        <TableCell align="center" className={"blue-cell " + ((row[0] && row[0].is_spy) ? "spy" : "")}>
                                            {row[0] ? row[0].name + (row[0].name === this.props.playerName ? " (me)" : "") : ""}
                                        </TableCell>
                                        <TableCell align="center" className={"red-cell " + ((row[1] && row[1].is_spy) ? "spy" : "")}>
                                            {row[1] ? row[1].name + (row[1].name === this.props.playerName ? " (me)" : "") : ""}
                                        </TableCell>
                                    </TableRow>
                                )
                            }
                        </TableBody>
                    </Table>
                </TableContainer>
            </div>
        );
    }
}

export default PlayerList;