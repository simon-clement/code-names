﻿import React, {Component} from "react";
import Grid from "@material-ui/core/Grid";
import PlayerList from "./PlayerList";
import ClueList from "./ClueList";
import ClueSender from "./ClueSender";
import GameStatus from "./GameStatus";

class BottomRow extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            sendClue: props.sendClue
        }
    }
    
    render() {
        return (
            <Grid container justify={"center"} spacing={3}>
                <Grid item xs={3}>
                    <PlayerList players={this.props.players} playerName={this.props.playerName}/>
                </Grid>
                {
                    this.props.registered &&
                    !this.props.resetting &&
                    <Grid container item xs={6} spacing={3}>
                        {
                            this.props.isSpy &&
                            this.props.playersTurn &&
                            <Grid container item xs={12}>
                                <ClueSender sendClue={this.state.sendClue}
                                            playersTurn={this.props.playersTurn}
                                            clueError={this.props.clueError}/>
                            </Grid>
                        }
                        <Grid container item xs={12}>
                            <GameStatus game={this.props.game}
                                        playersTurn={this.props.playersTurn}
                                        isSpy={this.props.isSpy}
                                        endTurn={this.props.endTurn}/>
                        </Grid>
                    </Grid>
                }
                {
                    this.props.registered &&
                    !this.props.resetting &&
                    <Grid item xs={3}>
                        <ClueList clues={this.props.clues}/>
                    </Grid>
                }
            </Grid>
        );
    }
}

export default BottomRow;