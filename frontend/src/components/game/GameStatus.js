﻿import React, {Component} from 'react';
import './GameStatus.css';
import Grid from "@material-ui/core/Grid";
import {GameStateEnum} from "./enums";
import Button from "@material-ui/core/Button";

class GameStatus extends Component {
    constructor(props) {
        super(props);

        this.status = this.status.bind(this);
        
        this.state = {
        };
    }
    
    status() {
        switch (this.props.game.state) {
            case GameStateEnum.BLUE_SPY_TURN:
                return <span>ATTENTE ESPION BLEU</span>;
            case GameStateEnum.BLUE_TURN:
                return <span>ATTENTE JOUEURS BLEUS</span>;
            case GameStateEnum.RED_SPY_TURN:
                return <span>ATTENTE ESPION ROUGE</span>;
            case GameStateEnum.RED_TURN:
                return <span>ATTENTE JOUEURS ROUGES</span>;
            case GameStateEnum.FINISHED:
                return <span className={this.props.game.blue_won ? "blue-text" : "red-text"}>
                    VICTOIRE DES {this.props.game.blue_won ? "BLEUS" : "ROUGES"}
                </span>;
        }
    }

    render() {
        return (
            <Grid container item justify="center" spacing={1} className="GameStatus">
                <Grid item xs={1}>
                    <span className={"blue-text"}>{this.props.game.remaining_blue}</span>
                </Grid>
                <Grid item xs={10}>
                    {this.status()}
                </Grid>
                <Grid item xs={1}>
                    <span className={"red-text"}>{this.props.game.remaining_red}</span>
                </Grid>
                {
                    this.props.playersTurn &&
                    !this.props.isSpy &&
                    <Grid item xs={7}>
                        <Button variant="contained" color="primary" onClick={this.props.endTurn}>Terminer le tour</Button>
                    </Grid>
                }
            </Grid>
        );
    }
}

export default GameStatus;
