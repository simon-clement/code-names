﻿import React, {Component} from "react";
import Grid from "@material-ui/core/Grid";
import {Button, Input} from "@material-ui/core";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import "./PlayerRegistration.css"

class PlayerRegistration extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            registering: false,
            name: '',
            color: '',
            err: false,
            sendPlayer: props.sendPlayer
        };
        
        this.register = this.register.bind(this);
    }
    
    register() {
        this.setState({registering: true, err: false});
        if (this.checkPlayer(this.state.name, this.state.color)) {
            this.state.sendPlayer(this.state.name, this.state.color);
        }
        else {
            this.setState({registering: false, err: true})
        }
    }
    
    checkPlayer(name, color) {
        return color && (color === 'blue' || color === 'red') && name && name.length > 0 && name.length <= 16;
    }
    
    componentDidUpdate(prevProps) {
        if (this.props.try !== prevProps.try) {
            this.setState({registering: false, err: true})
        }
    }

    render() {
        if (this.state.registering) {
            return (
                <Grid style={{width: "50%"}} container justify="center" spacing={1} className="PlayerRegistrationForm">
                    <Grid item xs={7} style={{height: "50px"}}/>
                    <Grid item xs={7}>
                        <span>Enregistrement...</span>
                    </Grid>
                    <Grid item xs={7} style={{height: "50px"}}/>
                </Grid>
            );
        }

        return (
            <Grid style={{width: "50%"}} container justify="center" spacing={1} className="PlayerRegistrationForm">
                <Grid item xs={10}>
                    {
                        this.state.err &&
                        <span style={{color: "red"}}>
                            Erreur : ce joueur existe déja ou les informations rentrées sont invalides.
                            Ton nom doit avoir entre 1 et 16 caractères, et n'oublie pas de choisir une couleur !
                        </span>
                    }
                </Grid>
                <Grid item xs={7}>
                    <span>Quel est ton nom ?</span>
                </Grid>
                <Grid item xs={7}>
                    <Input fullWidth onChange={(event) => this.setState({name: event.target.value})}/>
                </Grid>
                <Grid item xs={7}>
                    <span>Quelle est ta quete ?</span>
                </Grid>
                <Grid item xs={7}>
                    <RadioGroup aria-label="quest" name="quest1">
                        <FormControlLabel value="Le Graal !" control={<Radio color="primary"/>} label="Le Graal !" />
                    </RadioGroup>
                </Grid>
                <Grid item xs={7}>
                    <span>Quelle est ta couleur préférée ? (ton équipe)</span>
                </Grid>
                <Grid item xs={7}>
                    <RadioGroup aria-label="color" name="color1" onChange={(event) => this.setState({color: event.target.value})}>
                        <FormControlLabel value="blue" control={<Radio color="primary"/>} label="Bleu" />
                        <FormControlLabel value="red" control={<Radio color="primary"/>} label="Rouge" />
                    </RadioGroup>
                </Grid>
                <Grid item xs={7}>
                    <Button onClick={this.register} variant="contained" color="primary">
                        M'enregistrer
                    </Button>
                </Grid>
            </Grid>
        );
    }
}

export default PlayerRegistration;