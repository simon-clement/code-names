import React, {Component} from 'react';
import './Game.css';
import Board from "./Board";
import ReconnectingWebSocket from 'reconnecting-websocket'
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import PlayerRegistration from "./PlayerRegistration";
import BottomRow from "./BottomRow";
import {GameStateEnum, RequestTypeEnum} from "./enums";

class Game extends Component {
    constructor(props) {
        super(props);
        
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        const gameId = urlParams.get("game_id");
        const isSpy = false;

        this.onMessage = this.onMessage.bind(this);
        this.revealCard = this.revealCard.bind(this);
        this.setPlayers = this.setPlayers.bind(this);
        this.removePlayer = this.removePlayer.bind(this);
        this.addPlayer = this.addPlayer.bind(this);
        this.sendPlayer = this.sendPlayer.bind(this);
        this.sendReveal = this.sendReveal.bind(this);
        this.sendReset = this.sendReset.bind(this);
        this.sendClue = this.sendClue.bind(this);
        this.getCards = this.getCards.bind(this);
        this.getClues = this.getClues.bind(this);
        this.toggleSpy = this.toggleSpy.bind(this);
        this.endTurn = this.endTurn.bind(this);
        this.playersturn = this.playersTurn.bind(this);
        
        let ws_scheme = window.location.protocol === "https:" ? "wss" : "ws";
        let webSocket = new ReconnectingWebSocket(ws_scheme + '://' + window.location.host + "/ws/game/" + gameId + "/");
        
        webSocket.onmessage = this.onMessage;
        
        this.state = {
            cards: [],
            clues: [],
            players: [],
            playerName: '',
            playerColor: '',
            registerTry: 0,
            registered: false,
            loaded: false,
            gameId: gameId,
            isSpy: isSpy,
            webSocket: webSocket,
            game: null,
            clueError: false,
            resetting: false
        }
    }

    componentDidMount() {
        this.state.webSocket.send(JSON.stringify({
            type: RequestTypeEnum.GET_PLAYERS,
            game_id: this.state.gameId
        }));
    }
    
    onMessage(message) {
        let data = JSON.parse(message.data);
        if (data.game) {
            this.setState({game: data.game});
        }
        
        switch (data.type) {
            case RequestTypeEnum.REVEAL:
                this.revealCard(data);
                break;
            case RequestTypeEnum.RESET:
                if (data.error !== 1) {
                    if (data.resetting) {
                        this.setState({resetting: true});
                    }
                    else {
                        this.setState({
                            cards: data.cards,
                            clues: [],
                            isSpy: false,
                            players: this.state.players.map(p => {
                                p.is_spy = false;
                                return p;
                            }),
                            resetting: false
                        });
                    }
                }
                break;
            case RequestTypeEnum.REGISTER_PLAYER:
                this.addPlayer(data);
                break;
            case RequestTypeEnum.GET_PLAYERS:
                this.setPlayers(data);
                break;
            case RequestTypeEnum.REMOVE_PLAYER:
                this.removePlayer(data);
                break;
            case RequestTypeEnum.GET_CARDS:
                if (data.error !== 1) {
                    this.setState({cards: data.cards, loaded: true});
                }
                break;
            case RequestTypeEnum.TOGGLE_SPY:
                if (data.error !== 1) {
                    this.setState({players: this.state.players.map(p => {
                        if (p.name === data.name)
                            p.is_spy = data.is_spy;
                        return p;
                        })});
                }
                break;
            case RequestTypeEnum.GET_CLUES:
                if (data.error !== 1) {
                    this.setState({clues: data.clues});
                }
                break;
            case RequestTypeEnum.ADD_CLUE:
                this.addClue(data);
                break;
            default:
                break;
        }
    }
    
    revealCard(data) {
        if (data.error !== 1) {
            let newCards = this.state.cards;
            newCards.map(c => {
                if (c.word === data.word) {
                    c.revealed = true;
                }
                return c;
            });
    
            this.setState({
                cards: newCards
            });
        }
    }
    
    setPlayers(data) {
        if (data.error !== 1) {
            this.setState({
                players: data.players
            });
        }
    }

    removePlayer(data) {
        let players = this.state.players.filter(p => p.name !== data.name);
        this.setState({
            players: players
        });
    }
    
    addClue(data) {
        if (data.error !== 1) {
            let clues = this.state.clues;
            clues.push({
                game_id: this.state.gameId,
                word: data.word,
                number: data.number,
                color: data.color
            })
            
            this.setState({clues: clues});
        }
        else {
            this.setState({clueError: true})
        }
    }

    addPlayer(data) {
        if (data.error === 1) {
            this.setState({
                playerName: '',
                playerColor: '',
                registerTry: this.state.registerTry + 1
            });
            return;
        }
        
        if (data.self) {
            this.setState({playerName: data.player.name, playerColor: data.player.color, registered: true});
            this.getCards();
            this.getClues();
        }
        
        let players = this.state.players;
        players.push(data.player);
        this.setState({players: players});
    }

    getCards() {
        this.state.webSocket.send(JSON.stringify({
            type: RequestTypeEnum.GET_CARDS,
            game_id: this.state.gameId,
        }));
    }

    getClues() {
        this.state.webSocket.send(JSON.stringify({
            type: RequestTypeEnum.GET_CLUES,
            game_id: this.state.gameId,
        }));
    }
    
    sendPlayer(playerName, color) {
        this.state.webSocket.send(JSON.stringify({
            type: RequestTypeEnum.REGISTER_PLAYER,
            game_id: this.state.gameId,
            name: playerName,
            color: color
        }));
    }
    
    sendReveal(word) {
        this.state.webSocket.send(JSON.stringify({
            type: RequestTypeEnum.REVEAL,
            game_id: this.state.gameId,
            word: word
        }))
    }

    sendReset() {
        this.setState({resetting: true});
        this.state.webSocket.send(JSON.stringify({
            type: RequestTypeEnum.RESET,
            game_id: this.state.gameId
        }));
    }

    sendClue(word, number) {
        this.setState({clueError: false});
        this.state.webSocket.send(JSON.stringify({
            type: RequestTypeEnum.ADD_CLUE,
            game_id: this.state.gameId,
            word: word,
            number: number,
            color: this.state.playerColor
        }));
    }
    
    toggleSpy() {
        this.state.webSocket.send(JSON.stringify({
            type: RequestTypeEnum.TOGGLE_SPY,
            is_spy: !this.state.isSpy,
        }));
        this.setState({isSpy: !this.state.isSpy});
    }
    
    endTurn() {
        this.state.webSocket.send(JSON.stringify({
            type: RequestTypeEnum.END_TURN
        }));
    }
    
    playersTurn() {
        if (!this.state.game) {
            return false;
        }
        switch (this.state.game.state) {
            case GameStateEnum.BLUE_SPY_TURN:
                return this.state.isSpy && this.state.playerColor === "blue";
            case GameStateEnum.RED_SPY_TURN:
                return this.state.isSpy && this.state.playerColor === "red";
            case GameStateEnum.BLUE_TURN:
                return !this.state.isSpy && this.state.playerColor === "blue";
            case GameStateEnum.RED_TURN:
                return !this.state.isSpy && this.state.playerColor === "red";
            default:
                return false;
        }
    }

    render() {
        return (
            <div className="Game">
                {
                    !this.state.registered &&
                    <PlayerRegistration try={this.state.registerTry} sendPlayer={this.sendPlayer} />
                }
                {
                    this.state.registered &&
                    this.state.loaded &&
                    !this.state.resetting &&
                    <div>
                        <Board gameId={this.state.gameId}
                               showColor={this.state.game.state === GameStateEnum.FINISHED}
                               game={this.state.game}
                               cards={this.state.cards}
                               isSpy={this.state.isSpy}
                               playersTurn={this.playersTurn()}
                               sendReveal={this.sendReveal} />
                        <div style={{height: "50px"}}/>
                        <Grid container justify="center" spacing={3}>
                            <Grid item xs={4}>
                                <Button variant="contained" color="primary" onClick={this.toggleSpy}>Passer {this.state.isSpy ? "joueur" : "espion"}</Button>
                            </Grid>
                            <Grid item xs={4}>
                                <Button variant="contained" color="primary" onClick={this.sendReset}>Recommencer une partie</Button>
                            </Grid>
                        </Grid>
                    </div>
                }
                {
                    this.state.resetting &&
                    <Grid style={{width: "50%"}} container justify="center" spacing={1} className="ResettingPanel">
                        <Grid item xs={7} style={{height: "50px"}}/>
                        <Grid item xs={7}>
                            <span>Création d'une nouvelle partie...</span>
                        </Grid>
                        <Grid item xs={7} style={{height: "50px"}}/>
                    </Grid>
                }
                <div style={{height: "50px"}} />
                <BottomRow game={this.state.game}
                           players={this.state.players}
                           clues={this.state.clues}
                           sendClue={this.sendClue}
                           registered={this.state.registered}
                           isSpy={this.state.isSpy}
                           playerName={this.state.playerName}
                           playersTurn={this.playersTurn()}
                           endTurn={this.endTurn}
                           clueError={this.state.clueError}
                           resetting={this.state.resetting}/>
            </div>
        );
    }
}

export default Game;
