# Code Names

Jeu CodeNames en ligne

## Installation

- Cloner ce depot
- Creer un virtualenv Python (version testee : 3.7) et y installer les dependances :
```bash
virtualenv venv
pip install -r requirements.txt
```
- Creer la base de donnees :
```bash
python manage.py migrate
```
- Installer les dependances NodeJs :
```bash
cd frontend
npm install
```
- Compiler les composants React :
```bash
npm run dev
```
- Lancer le serveur :
```bash
cd ..
python manage.py runserver
```

**Note :** Plus de parametrage est necessaire pour pouvoir acceder a l'application via d'autres appareils.

## Mots jouables

La base de données créée par le projet est de base vide. Il faut donc y insérer les
mots jouables pour que l'application fonctionne.

Le fichier `words` contient 694 mots francais, ils peuvent etre utilisés pour
peupler la base de mots du jeu. La table a peupler avec les mots jouables est
la table `cards_word`.
