from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from rest_framework import mixins, status

from .serializers import CardSerializer
from .utils.game_generator import generate_game
from .utils.game_manager import game_exists


class CreateGame(mixins.CreateModelMixin,
                 GenericAPIView):
    game_id = ''
    serializer_class = CardSerializer

    def post(self, request, *args, **kwargs):
        game_id = request.data.get('game_id', '')
        if game_exists(game_id):
            return Response("Game already exists", status=status.HTTP_400_BAD_REQUEST)

        generate_game(game_id)

        return Response(status=status.HTTP_201_CREATED)
