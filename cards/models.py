from django.db import models


class Word(models.Model):
    word = models.CharField(max_length=15)


class Card(models.Model):
    game_id = models.CharField(max_length=32)
    word = models.CharField(max_length=15)
    color = models.CharField(max_length=7)
    revealed = models.BooleanField(default=False)


class Player(models.Model):
    game_id = models.CharField(max_length=32)
    name = models.CharField(max_length=16)
    color = models.CharField(max_length=4)
    is_admin = models.BooleanField(default=False)
    is_spy = models.BooleanField(default=False)


class Clue(models.Model):
    game_id = models.CharField(max_length=32)
    word = models.CharField(max_length=32)
    number = models.IntegerField()
    color = models.CharField(max_length=4)
    played = models.IntegerField(default=0)


class Game(models.Model):
    game_id = models.CharField(max_length=32)
    blue_starting = models.BooleanField()
    remaining_blue = models.IntegerField()
    remaining_red = models.IntegerField()
    state = models.IntegerField()
    blue_won = models.BooleanField(null=True)
