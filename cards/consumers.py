﻿import json

from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer

from cards.consumer_requests.request_types import RequestTypeEnum

from cards.consumer_requests.add_clue import AddClueConsumer
from cards.consumer_requests.end_turn import EndTurnConsumer
from cards.consumer_requests.get_cards import GetCardsConsumer
from cards.consumer_requests.get_clues import GetCluesConsumer
from cards.consumer_requests.get_players import GetPlayersConsumer
from cards.consumer_requests.register_name import RegisterNameConsumer
from cards.consumer_requests.remove_player import RemovePlayerConsumer
from cards.consumer_requests.reset import ResetConsumer
from cards.consumer_requests.reveal import RevealConsumer
from cards.consumer_requests.toggle_spy import ToggleSpyConsumer

from cards.utils.player_manager import remove_player, player_exists


class CardConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.game_id = self.scope['url_route']['kwargs']['game_id']
        self.game_group_name = 'game_%s' % self.game_id

        self.player_name = ''
        self.color = ''

        # Join room group
        await self.channel_layer.group_add(
            self.game_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        # Remove player from database if it has been registered
        if await database_sync_to_async(player_exists)(self.game_id, self.player_name):
            await database_sync_to_async(remove_player)(self.game_id, self.player_name)

        # Send message to room group
        await self.channel_layer.group_send(
            self.game_group_name,
            {
                'type': 'respond',
                'res_type': RequestTypeEnum.REMOVE_PLAYER,
                'name': self.player_name
            }
        )

        # Leave room group
        await self.channel_layer.group_discard(
            self.game_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        request_type = text_data_json['type']
        if not self.validate_call(text_data_json):
            await self.send(text_data=json.dumps({
                'type': request_type,
                'error': 1
            }))
            return
        if request_type == RequestTypeEnum.REVEAL.value:
            await RevealConsumer.treat_call(self, text_data_json)
        elif request_type == RequestTypeEnum.RESET.value:
            await ResetConsumer.treat_call(self, text_data_json)
        elif request_type == RequestTypeEnum.REGISTER_PLAYER.value:
            await RegisterNameConsumer.treat_call(self, text_data_json)
        elif request_type == RequestTypeEnum.GET_PLAYERS.value:
            await GetPlayersConsumer.treat_call(self, text_data_json)
        elif request_type == RequestTypeEnum.GET_CARDS.value:
            await GetCardsConsumer.treat_call(self, text_data_json)
        elif request_type == RequestTypeEnum.TOGGLE_SPY.value:
            await ToggleSpyConsumer.treat_call(self, text_data_json)
        elif request_type == RequestTypeEnum.GET_CLUES.value:
            await GetCluesConsumer.treat_call(self, text_data_json)
        elif request_type == RequestTypeEnum.ADD_CLUE.value:
            await AddClueConsumer.treat_call(self, text_data_json)
        elif request_type == RequestTypeEnum.END_TURN.value:
            await EndTurnConsumer.treat_call(self, text_data_json)

    # Respond to a broadcast
    async def respond(self, event):
        res_type = event['res_type']
        if res_type == RequestTypeEnum.REVEAL:
            await RevealConsumer.respond(self, event)
        elif res_type == RequestTypeEnum.RESET:
            await ResetConsumer.respond(self, event)
        elif res_type == RequestTypeEnum.REGISTER_PLAYER:
            await RegisterNameConsumer.respond(self, event)
        elif res_type == RequestTypeEnum.REMOVE_PLAYER:
            await RemovePlayerConsumer.respond(self, event)
        elif res_type == RequestTypeEnum.TOGGLE_SPY:
            await ToggleSpyConsumer.respond(self, event)
        elif res_type == RequestTypeEnum.ADD_CLUE:
            await AddClueConsumer.respond(self, event)
        elif res_type == RequestTypeEnum.END_TURN:
            await EndTurnConsumer.respond(self, event)

    async def validate_call(self, text_data_json):
        request_type = text_data_json['type']
        if request_type == RequestTypeEnum.REVEAL.value:
            return await RevealConsumer.valid_call(self, text_data_json)
        elif request_type == RequestTypeEnum.RESET.value:
            return await ResetConsumer.valid_call(self, text_data_json)
        elif request_type == RequestTypeEnum.REGISTER_PLAYER.value:
            return await RegisterNameConsumer.valid_call(self, text_data_json)
        elif request_type == RequestTypeEnum.GET_PLAYERS.value:
            return await GetPlayersConsumer.valid_call(self, text_data_json)
        elif request_type == RequestTypeEnum.GET_CARDS.value:
            return await GetCardsConsumer.valid_call(self, text_data_json)
        elif request_type == RequestTypeEnum.TOGGLE_SPY.value:
            return await ToggleSpyConsumer.valid_call(self, text_data_json)
        elif request_type == RequestTypeEnum.GET_CLUES.value:
            return await GetCluesConsumer.valid_call(self, text_data_json)
        elif request_type == RequestTypeEnum.ADD_CLUE.value:
            return await AddClueConsumer.valid_call(self, text_data_json)
        elif request_type == RequestTypeEnum.END_TURN.value:
            return await EndTurnConsumer.valid_call(self, text_data_json)
