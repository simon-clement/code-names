from rest_framework import serializers
from .models import Word, Card, Player, Clue, Game


class WordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Word
        fields = ['word']


class CardSerializer(serializers.ModelSerializer):
    class Meta:
        model = Card
        fields = ('game_id', 'word', 'color', 'revealed')


class PlayerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Player
        fields = ('game_id', 'name', 'color', 'is_admin', 'is_spy')


class ClueSerializer(serializers.ModelSerializer):
    class Meta:
        model = Clue
        fields = ('game_id', 'word', 'number', 'color')


class GameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Game
        fields = ('game_id', 'remaining_blue', 'remaining_red', 'state', 'blue_won')
