﻿import json
import re

from channels.db import database_sync_to_async

from cards.consumer_requests.consumer_request import ConsumerRequest
from cards.consumer_requests.request_types import RequestTypeEnum
from cards.utils.clue_manager import add_clue
from cards.utils.game_manager import get_game, set_state
from cards.utils.game_states import GameStateEnum


class AddClueConsumer(ConsumerRequest):
    @staticmethod
    async def treat_call(consumer, text_data_json):
        word = text_data_json['word']
        number = text_data_json['number']
        color = consumer.color

        await database_sync_to_async(add_clue)(consumer.game_id, word, number, color)

        await database_sync_to_async(set_state)(consumer.game_id,
                                                GameStateEnum.BLUE_TURN if consumer.color == "blue" else
                                                GameStateEnum.RED_TURN)

        # Send message to room group
        await consumer.channel_layer.group_send(
            consumer.game_group_name,
            {
                'type': 'respond',
                'res_type': RequestTypeEnum.ADD_CLUE,
                'word': word,
                'number': number,
                'color': color
            }
        )

    @staticmethod
    async def respond(consumer, event):
        word = event['word']
        number = event['number']
        color = event['color']
        game = await database_sync_to_async(get_game)(consumer.game_id, True)

        # Send message to WebSocket
        await consumer.send(text_data=json.dumps({
            'type': RequestTypeEnum.ADD_CLUE.value,
            'word': word,
            'number': number,
            'color': color,
            'game': game
        }))

    @staticmethod
    async def valid_call(consumer, text_data_json):
        return 'word' in text_data_json and 'number' in text_data_json and \
               re.match(r"^[^ \t\n\r\f\v_.]+$", text_data_json['word']) and \
               (text_data_json['number'] == -1 or 0 < text_data_json['number'] < 10)
