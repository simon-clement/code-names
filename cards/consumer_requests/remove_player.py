﻿import json

from channels.db import database_sync_to_async

from cards.consumer_requests.consumer_request import ConsumerRequest
from cards.consumer_requests.request_types import RequestTypeEnum
from cards.utils.game_manager import get_game


class RemovePlayerConsumer(ConsumerRequest):
    @staticmethod
    async def treat_call(consumer, text_data_json):
        pass

    @staticmethod
    async def respond(consumer, event):
        name = event['name']
        game = await database_sync_to_async(get_game)(consumer.game_id, True)

        # Send message to WebSocket
        await consumer.send(text_data=json.dumps({
            'type': RequestTypeEnum.REMOVE_PLAYER.value,
            'name': name
        }))

    @staticmethod
    async def valid_call(consumer, text_data_json):
        return True
