﻿from abc import ABC, abstractmethod


class ConsumerRequest(ABC):
    def __init__(self):
        super().__init__()

    @staticmethod
    @abstractmethod
    def treat_call(consumer, text_data_json):
        pass

    @staticmethod
    @abstractmethod
    def respond(consumer, event):
        pass

    @staticmethod
    @abstractmethod
    def valid_call(consumer, text_data_json):
        pass
