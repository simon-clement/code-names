﻿from channels.db import database_sync_to_async

import json

from cards.consumer_requests.consumer_request import ConsumerRequest
from cards.consumers import RequestTypeEnum
from cards.utils.game_manager import get_game, set_state
from cards.utils.game_states import GameStateEnum


class EndTurnConsumer(ConsumerRequest):
    @staticmethod
    async def treat_call(consumer, text_data_json):
        game = await database_sync_to_async(get_game)(consumer.game_id)
        if game.state == GameStateEnum.BLUE_TURN.value:
            await database_sync_to_async(set_state)(consumer.game_id, GameStateEnum.RED_SPY_TURN)
        else:
            await database_sync_to_async(set_state)(consumer.game_id, GameStateEnum.BLUE_SPY_TURN)

        # Send message to room group
        await consumer.channel_layer.group_send(
            consumer.game_group_name,
            {
                'type': 'respond',
                'res_type': RequestTypeEnum.END_TURN
            }
        )

    @staticmethod
    async def respond(consumer, event):
        game = await database_sync_to_async(get_game)(consumer.game_id, True)

        # Send message to WebSocket
        await consumer.send(text_data=json.dumps({
            'type': RequestTypeEnum.END_TURN.value,
            'game': game
        }))

    @staticmethod
    async def valid_call(consumer, text_data_json):
        game = await database_sync_to_async(get_game)(consumer.game_id)
        return (game.state == GameStateEnum.BLUE_TURN.value and consumer.color == "blue") or \
               (game.state == GameStateEnum.RED_TURN.value and consumer.color == "red")
