﻿import json

from channels.db import database_sync_to_async

from cards.consumer_requests.consumer_request import ConsumerRequest
from cards.consumer_requests.request_types import RequestTypeEnum
from cards.utils.clue_manager import get_last_clue, increment_clue_played
from cards.utils.game_manager import db_reveal_word, decrement_blue, decrement_red, get_game, set_win, set_state
from cards.utils.game_states import GameStateEnum


class RevealConsumer(ConsumerRequest):
    @staticmethod
    async def treat_call(consumer, text_data_json):
        word = text_data_json['word']

        card = await database_sync_to_async(db_reveal_word)(consumer.game_id, word)
        if card is None:  # tried to reveal a non-existing or already revealed card
            return
        game = await database_sync_to_async(get_game)(consumer.game_id)
        clue = await database_sync_to_async(get_last_clue)(consumer.game_id)

        blue_win = False
        red_win = False
        black_win = False
        if card.color == "blue":
            blue_win = await database_sync_to_async(decrement_blue)(game)
        elif card.color == "red":
            red_win = await database_sync_to_async(decrement_red)(game)
        elif card.color == "black":
            black_win = True

        if blue_win:
            await database_sync_to_async(set_win)(consumer.game_id, True)
        elif red_win:
            await database_sync_to_async(set_win)(consumer.game_id, False)
        elif black_win:
            await database_sync_to_async(set_win)(consumer.game_id, game.state == GameStateEnum.RED_TURN.value)
        else:
            if game.state == GameStateEnum.BLUE_TURN.value:
                if card.color != "blue" or clue.played == clue.number:
                    await database_sync_to_async(set_state)(consumer.game_id, GameStateEnum.RED_SPY_TURN)
                else:
                    await database_sync_to_async(increment_clue_played)(clue)
            elif game.state == GameStateEnum.RED_TURN.value:
                if card.color != "red" or clue.played == clue.number:
                    await database_sync_to_async(set_state)(consumer.game_id, GameStateEnum.BLUE_SPY_TURN)
                else:
                    await database_sync_to_async(increment_clue_played)(clue)

        # Send message to room group
        await consumer.channel_layer.group_send(
            consumer.game_group_name,
            {
                'type': 'respond',
                'res_type': RequestTypeEnum.REVEAL,
                'word': word
            }
        )

    @staticmethod
    async def respond(consumer, event):
        word = event['word']
        game = await database_sync_to_async(get_game)(consumer.game_id, True)

        # Send message to WebSocket
        await consumer.send(text_data=json.dumps({
            'type': RequestTypeEnum.REVEAL.value,
            'word': word,
            'game': game
        }))

    @staticmethod
    async def valid_call(consumer, text_data_json):
        return 'word' in text_data_json
