﻿from channels.db import database_sync_to_async

import json

from cards.consumer_requests.consumer_request import ConsumerRequest
from cards.consumer_requests.request_types import RequestTypeEnum
from cards.utils.clue_manager import get_clues
from cards.utils.game_manager import get_game


class GetCluesConsumer(ConsumerRequest):
    @staticmethod
    async def treat_call(consumer, text_data_json):
        game = await database_sync_to_async(get_game)(consumer.game_id, True)
        clues = await database_sync_to_async(get_clues)(consumer.game_id, True)

        # Send message to WebSocket
        await consumer.send(text_data=json.dumps({
            'type': RequestTypeEnum.GET_CLUES.value,
            'clues': clues,
            'game': game
        }))

    @staticmethod
    async def respond(consumer, event):
        pass

    @staticmethod
    async def valid_call(consumer, text_data_json):
        return True
