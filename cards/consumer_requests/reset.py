﻿from asgiref.sync import sync_to_async
from channels.db import database_sync_to_async

import json

from cards.consumer_requests.consumer_request import ConsumerRequest
from cards.consumers import RequestTypeEnum
from cards.serializers import CardSerializer
from cards.utils.game_generator import generate_game
from cards.utils.game_manager import delete_game, get_game_cards, get_game
from cards.utils.player_manager import reset_spy


class ResetConsumer(ConsumerRequest):
    @staticmethod
    async def treat_call(consumer, text_data_json):
        # Send message to hide Reset button while resetting
        await consumer.channel_layer.group_send(
            consumer.game_group_name,
            {
                'type': 'respond',
                'res_type': RequestTypeEnum.RESET,
                'resetting': True
            }
        )

        await database_sync_to_async(delete_game)(consumer.game_id)
        await database_sync_to_async(reset_spy)(consumer.game_id)
        await database_sync_to_async(generate_game)(consumer.game_id)

        # Send message to room group
        await consumer.channel_layer.group_send(
            consumer.game_group_name,
            {
                'type': 'respond',
                'res_type': RequestTypeEnum.RESET,
                'resetting': False
            }
        )

    @staticmethod
    async def respond(consumer, event):
        if event['resetting']:
            await consumer.send(text_data=json.dumps({
                'type': RequestTypeEnum.RESET.value,
                'resetting': True
            }))
        else:
            cards = await database_sync_to_async(get_game_cards)(consumer.game_id, True)
            game = await database_sync_to_async(get_game)(consumer.game_id, True)

            # Send message to WebSocket
            await consumer.send(text_data=json.dumps({
                'type': RequestTypeEnum.RESET.value,
                'cards': cards,
                'game': game,
                'resetting': False
            }))

    @staticmethod
    async def valid_call(consumer, text_data_json):
        return True
