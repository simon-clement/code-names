﻿import json

from channels.db import database_sync_to_async

from cards.consumer_requests.consumer_request import ConsumerRequest
from cards.consumer_requests.request_types import RequestTypeEnum
from cards.utils.game_manager import get_game
from cards.utils.player_manager import toggle_spy


class ToggleSpyConsumer(ConsumerRequest):
    @staticmethod
    async def treat_call(consumer, text_data_json):
        name = consumer.player_name
        is_spy = text_data_json['is_spy']

        await database_sync_to_async(toggle_spy)(consumer.game_id, name, is_spy)

        # Send message to room group
        await consumer.channel_layer.group_send(
            consumer.game_group_name,
            {
                'type': 'respond',
                'res_type': RequestTypeEnum.TOGGLE_SPY,
                'name': name,
                'is_spy': is_spy
            }
        )

    @staticmethod
    async def respond(consumer, event):
        name = event['name']
        is_spy = event['is_spy']
        game = await database_sync_to_async(get_game)(consumer.game_id, True)

        # Send message to WebSocket
        await consumer.send(text_data=json.dumps({
            'type': RequestTypeEnum.TOGGLE_SPY.value,
            'name': name,
            'is_spy': is_spy,
            'game': game
        }))

    @staticmethod
    async def valid_call(consumer, text_data_json):
        return 'is_spy' in text_data_json
