﻿import json

from channels.db import database_sync_to_async

from cards.consumer_requests.consumer_request import ConsumerRequest
from cards.consumer_requests.request_types import RequestTypeEnum
from cards.utils.game_manager import get_game
from cards.utils.player_manager import add_player, get_player, player_exists


class RegisterNameConsumer(ConsumerRequest):
    @staticmethod
    async def treat_call(consumer, text_data_json):
        name = text_data_json['name']
        color = text_data_json['color']

        if await database_sync_to_async(player_exists)(consumer.game_id, name):
            await consumer.send(text_data=json.dumps({
                'type': RequestTypeEnum.REGISTER_PLAYER.value,
                'error': 1
            }))
            return

        consumer.player_name = name
        consumer.color = color

        await database_sync_to_async(add_player)(consumer.game_id, consumer.player_name, consumer.color)

        # Send message to room group
        await consumer.channel_layer.group_send(
            consumer.game_group_name,
            {
                'type': 'respond',
                'res_type': RequestTypeEnum.REGISTER_PLAYER,
                'player_name': consumer.player_name,
            }
        )

    @staticmethod
    async def respond(consumer, event):
        player_name = event['player_name']

        game = await database_sync_to_async(get_game)(consumer.game_id, True)
        player = await database_sync_to_async(get_player)(consumer.game_id, player_name, True)

        # Send message to WebSocket
        await consumer.send(text_data=json.dumps({
            'type': RequestTypeEnum.REGISTER_PLAYER.value,
            'error': 0,
            'self': player_name == consumer.player_name,
            'player': player,
            'game': game
        }))

    @staticmethod
    async def valid_call(consumer, text_data_json):
        return 'name' in text_data_json and 'color' in text_data_json and \
               0 < len(text_data_json['name']) <= 16 and text_data_json['color'] in ['blue', 'red']
