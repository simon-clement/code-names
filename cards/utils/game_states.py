﻿from enum import Enum


class GameStateEnum(Enum):
    BLUE_SPY_TURN = 1
    RED_SPY_TURN = 2
    BLUE_TURN = 3
    RED_TURN = 4
    FINISHED = 5
