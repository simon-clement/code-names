﻿from cards.models import Card, Game
from cards.serializers import CardSerializer, GameSerializer
from cards.utils.clue_manager import empty_clues
from cards.utils.game_states import GameStateEnum


def game_exists(game_id):
    return len(Card.objects.filter(game_id=game_id)) > 0


def get_game_cards(game_id, serialized=False):
    cards = Card.objects.filter(game_id=game_id).order_by('id')
    if serialized:
        return CardSerializer(cards, many=True).data
    else:
        return cards


def get_game(game_id, serialized=False):
    game = Game.objects.filter(game_id=game_id)[0]
    if serialized:
        return GameSerializer(game).data
    else:
        return game


def delete_game(game_id):
    Card.objects.filter(game_id=game_id).delete()
    Game.objects.filter(game_id=game_id).delete()
    empty_clues(game_id)


def db_reveal_word(game_id, word):
    queryset = Card.objects.filter(game_id=game_id, word=word)
    if len(queryset) == 0 or queryset[0].revealed:
        return None
    queryset.update(revealed=True)
    return queryset[0]


def decrement_blue(game):
    current = game.remaining_blue
    Game.objects.filter(game_id=game.game_id).update(remaining_blue=current-1)
    return current <= 1


def decrement_red(game):
    current = game.remaining_red
    Game.objects.filter(game_id=game.game_id).update(remaining_red=current-1)
    return current <= 1


def set_win(game_id, blue_won):
    Game.objects.filter(game_id=game_id).update(state=GameStateEnum.FINISHED.value, blue_won=blue_won)


def set_state(game_id, state):
    Game.objects.filter(game_id=game_id).update(state=state.value)
