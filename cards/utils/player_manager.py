﻿from cards.models import Player
from cards.serializers import PlayerSerializer


def player_exists(game_id, player_name):
    player = Player.objects.filter(game_id=game_id, name=player_name)
    return len(player) > 0


def add_player(game_id, player_name, color):
    Player.objects.create(game_id=game_id, name=player_name, color=color)


def remove_player(game_id, player_name):
    Player.objects.filter(game_id=game_id, name=player_name).delete()


def get_players(game_id, serialized=False):
    players = Player.objects.filter(game_id=game_id)
    if serialized:
        return PlayerSerializer(players, many=True).data
    return players


def get_player(game_id, player_name, serialized=False):
    player = Player.objects.filter(game_id=game_id, name=player_name)
    if serialized:
        return PlayerSerializer(player[0]).data
    return player


def toggle_spy(game_id, player_name, is_spy):
    Player.objects.filter(game_id=game_id, name=player_name).update(is_spy=is_spy)


def reset_spy(game_id):
    Player.objects.filter(game_id=game_id).update(is_spy=False)
