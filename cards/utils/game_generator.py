﻿import random

from cards.models import Word, Card, Game
from cards.utils.game_states import GameStateEnum


def generate_game(game_id):
    words = generate_words()
    color_map, blue_starts = generate_color_map()

    create_cards(game_id, words, color_map)
    Game.objects.create(game_id=game_id,
                        blue_starting=blue_starts,
                        remaining_blue=(9 if blue_starts else 8),
                        remaining_red=(8 if blue_starts else 9),
                        state=(GameStateEnum.BLUE_SPY_TURN.value if blue_starts else GameStateEnum.RED_SPY_TURN.value))


def generate_words():
    words = Word.objects.all()
    return list(map(lambda w: w.word, random.sample(list(words), k=25)))


def generate_color_map():
    blue_starts = random.randint(0, 1)
    color_map = ["black"] + ["red"] * 8 + ["blue"] * 8 + ["neutral"] * 7 + \
                (["blue"] if blue_starts else ["red"])
    random.shuffle(color_map)
    return color_map, blue_starts


def create_cards(game_id, words, color_map):
    for i in range(len(words)):
        Card.objects.create(game_id=game_id, word=words[i], color=color_map[i])
