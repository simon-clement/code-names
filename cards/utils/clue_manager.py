﻿from cards.models import Clue
from cards.serializers import ClueSerializer


def get_clues(game_id, serialized=False):
    clues = Clue.objects.filter(game_id=game_id)
    if serialized:
        return ClueSerializer(clues, many=True).data
    return clues


def empty_clues(game_id):
    Clue.objects.filter(game_id=game_id).delete()


def add_clue(game_id, word, number, color):
    Clue.objects.create(game_id=game_id, word=word, number=number, color=color)


def get_last_clue(game_id):
    return Clue.objects.filter(game_id=game_id).order_by('-id')[0]


def increment_clue_played(clue):
    Clue.objects.filter(id=clue.id).update(played=clue.played+1)
